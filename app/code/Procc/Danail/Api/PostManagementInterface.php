<?php

namespace Procc\Danail\Api;


interface PostManagementInterface
{


    /**
     * GET for Post api
     * @param string $param
     * @return string
     */

    public function changeCategoryStatus();


    /**
     * Change Status for changeProductStatus api
     * @param string $param
     * @return string
     */
    public function changeProductStatus();


    /**
     * Delete Store for deleteStore api
     * @param string $param
     * @return string
     */

    public function deleteStoreWithStoreName();

    /**
     * Disable Store for disableStore api
     * @param string $param
     * @return string
     */

    public function disableStoreWithStoreName();


    /**
     * Create Store for createStore api
     * @param string $param
     * @return string
     */
    public function createStoreView();

    /**
     * Update Product Qty using rest api
     * @return string
     */
    public function productUpdateQty();

    /**
     * Update Store Indexer using rest api
     * @return string
     */
    public function addNewStoreForReIndexing();

    /**
     * Reindex Store using rest api
     * @return string
     */
    public function reIndexStoreUsingStoreCode();

}
