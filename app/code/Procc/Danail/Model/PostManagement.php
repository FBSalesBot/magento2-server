<?php

namespace Procc\Danail\Model;

use Magento\Framework\App\Bootstrap;

class PostManagement
{
    /**
     * {@inheritdoc}
     */
    public function changeCategoryStatus()
    {

        $request_data = json_decode(file_get_contents('php://input'), true);
        $category_id = $request_data['category_id'];
        $status = $request_data['status'];
        $storename = $request_data['store_name'];

        ini_set('display_errors', 1);
        error_reporting(E_ALL | E_STRICT);

        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $obj->get('Magento\Framework\Registry')->register('isSecureArea', true);


        $storeModel1 = $obj->get('Magento\Store\Model\ResourceModel\Website');

        $storeModel = $obj->create('Magento\Store\Model\Website');
        $checkwebsite = $storeModel1->readAllWebsites();

        if (array_key_exists($storename, $checkwebsite)) {
            $websiteData = $checkwebsite[$storename];

            //echo $websiteData['website_id'];
            $category = $obj->create('Magento\Catalog\Model\CategoryFactory')->create()->setStoreId($websiteData['website_id'])->load($category_id);

            //print_r($category->getData());
            $category->setIsActive($status);
            $category->save();
            //print_r($category->getData());
            if ($status == 0)
                echo "Category disabled successfully";
            else
                echo "Category enabled successfully";

            exit;
        }

    }

    public function productUpdateQty()
    {
        $request_data = json_decode(file_get_contents('php://input'), true);
        $product_id =  isset($request_data['product_id']) ? $request_data['product_id'] : '';
        $product_sku = isset($request_data['product_sku']) ? $request_data['product_sku'] : '';
        $qty = $request_data['qty'];

        if($product_id == '' && $product_sku == '')
        {
            echo "Please provide product id/sku";
            exit;
        }
        else
        {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
            $product = '';

            if($product_id !== ''){
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
            }

            if($product_sku !== ''){
                $product = $objectManager->get('Magento\Catalog\Model\Product')->loadByAttribute('sku',$product_sku);
            }

            $qtyData = $StockState->getStockQty($product->getId());
            $stockRegistry = $objectManager->create('Magento\CatalogInventory\Api\StockRegistryInterface');
            $stockItem = $stockRegistry->getStockItem($product->getId());
            $qtyData=$qtyData+$qty;
            $stockItem->setData('qty',$qtyData);
            $stockItem->save();

            echo "Product quantity updated successfully";
            exit;
        }
    }

    public function addNewStoreForReIndexing()
    {
        $request_data = json_decode(file_get_contents('php://input'), true);
        $store_id =  isset($request_data['store_id']) ? $request_data['store_id'] : '';
        if($store_id == '' )
        {
            echo "Please provide product store id";
            exit;
        }
        else {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

            echo $indexer_setting=$objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface')->getValue('vsbridge_indexer_settings/general_settings/allowed_stores');

            $cacheTypeList=$objectManager->get('Magento\Framework\App\Cache\TypeListInterface');
            $cacheTypeList->cleanType('config');

            $temp=explode(",",$indexer_setting);

            if(!in_array($store_id,$temp)) {

                $indexer_setting = $indexer_setting . "," . $store_id;
                $objectManager->get('\Magento\Config\Model\ResourceModel\Config')->saveConfig('vsbridge_indexer_settings/general_settings/allowed_stores', $indexer_setting);

                echo "Vue Store Indexer enabled successfully";
                exit;
            }
            else
            {
                echo "Vue Store Indexer already selected ";exit;
            }
        }
    }

    public function changeProductStatus()
    {
        $request_data = json_decode(file_get_contents('php://input'), true);
        $product_id = $request_data['product_id'];
        $status = $request_data['status'];
        $storename = $request_data['store_name'];

        ini_set('display_errors', 1);
        error_reporting(E_ALL | E_STRICT);


        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $obj->get('Magento\Framework\Registry')->register('isSecureArea', true);


        $storeModel1 = $obj->get('Magento\Store\Model\ResourceModel\Website');


        $checkwebsite = $storeModel1->readAllWebsites();

        if (array_key_exists($storename, $checkwebsite)) {
            $websiteData = $checkwebsite[$storename];

            $productCollection = $obj->create('\Magento\Catalog\Model\ProductFactory')->create()->setStoreId($websiteData['website_id'])->load($product_id);;

            $productCollection->setStatus($status);
            $productCollection->save();

            if ($status == 0)
                echo "Product disabled successfully";
            else
                echo "Product enabled successfully";

            exit;
        }
    }

    public function deleteStoreWithStoreName()
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $state = $obj->get('Magento\Framework\App\State');

        $request_data = json_decode(file_get_contents('php://input'), true);

        $storename = $request_data['store_name'];

        $storeManager = $obj->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteFactory = $obj->get('\Magento\Store\Model\WebsiteFactory');


        $storeModel1 = $obj->get('Magento\Store\Model\ResourceModel\Website');

        $storeModel = $obj->create('Magento\Store\Model\Website');
        $checkwebsite = $storeModel1->readAllWebsites();

        if (array_key_exists($storename, $checkwebsite)) {
            //   $websiteData=$checkwebsite[$storename];

            $obj->get('Magento\Framework\Registry')->register('isSecureArea', true);


            $websiterepo = $obj->create('Magento\Store\Model\WebsiteRepository');
            $website = $websiterepo->get($storename);
            $website->getResource()->delete($website);

            echo "Store deleted successfully";

        } else {
            echo "Store " . $storename . " does not exists";
        }

        exit;

    }

    public function createStoreView()
    {

        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $request_data = json_decode(file_get_contents('php://input'), true);
        $storename = $request_data['store_name'];
        $storecode = $request_data['store_code'];
        $root_category_id = $request_data['root_category_id'];

        $state = $obj->get('Magento\Framework\App\State');
        $storeManager = $obj->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteFactory = $obj->get('\Magento\Store\Model\WebsiteFactory');

        $groupResourceModel = $obj->get('Magento\Store\Model\ResourceModel\Group');
        $storeFactory = $obj->get('Magento\Store\Model\StoreFactory');
        $groupFactory = $obj->get('Magento\Store\Model\GroupFactory');
        $eventManager = $obj->get('Magento\Framework\Event');
        $storeModel1 = $obj->get('Magento\Store\Model\ResourceModel\Website');

        $storeModel = $obj->create('Magento\Store\Model\Store');
        $checkwebsite = $storeModel1->readAllWebsites();

        if (array_key_exists($storename, $checkwebsite)) {
            echo json_encode($checkwebsite[$storename]);
        } else {
            $new_store = $websiteFactory->create();
            $new_store->setName($storename);
            $new_store->setCode($storecode);

            $new_store->save();

            if ($new_store->getId()) {
                $group = $groupFactory->create();
                $group->setWebsiteId($new_store->getWebsiteId());
                $group->setName($storename);
                $group->setCode($storecode);
                $group->setRootCategoryId($root_category_id);
                $group->setDefaultStoreId(3);
                $group->save();
            }

            $store = $storeFactory->create();
            $store->load($storename);

            if (!$store->getId()) {
                $group = $groupFactory->create();
                $group->load($storename, 'name');
                $store->setCode($storecode);
                $store->setName($storename);
                $store->setWebsite($new_store);
                $store->setGroupId($group->getId());
                $store->setData('is_active', '1');
                $store->save($store);

                $storeDetails = json_encode($store->getData());
                echo $storeDetails;
                exit;
            }
        }

    }

    public function disableStoreWithStoreName()
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();

        $state = $obj->get('Magento\Framework\App\State');

        $request_data = json_decode(file_get_contents('php://input'), true);

        $storename = $request_data['store_name'];
        $storestatus = $request_data['status'];

        $storeManager = $obj->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteFactory = $obj->get('\Magento\Store\Model\WebsiteFactory');


        $storeModel1 = $obj->get('Magento\Store\Model\ResourceModel\Website');

        $storeModel = $obj->create('Magento\Store\Model\Website');
        $checkwebsite = $storeModel1->readAllWebsites();

        if (array_key_exists($storename, $checkwebsite)) {
            $websiteData=$checkwebsite[$storename];

            $obj->get('Magento\Framework\Registry')->register('isSecureArea', true);

            $websiterepo = $obj->create('Magento\Store\Model\Store');
            $websiterepo->load($websiteData['website_id']);
            $websiterepo->setIsActive($storestatus);
            $websiterepo->save();

            if($storestatus === 0)
                echo "Store disabled successfully";
            else
                echo "Store enabled successfully";

        } else {
            echo "Store " . $storename . " does not exists";
        }
        exit;
    }

    public function reIndexStoreUsingStoreCode(){
        $request_data = json_decode(file_get_contents('php://input'), true);
        $storecode = $request_data['store_code'];
        $command = 'php bin/magento vsbridge:reindex --store='.$storecode;
        $output = shell_exec($command);
        echo $output;
        exit;
    }
}
